<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<style>
     #formId {
            margin-top:20px;
            margin-left:50px;
            margin-right:1200px;
     }
</style>
<head>
    <title>Edit</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css"
            integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
</head>
<body>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">MAIN</a></li>
        <li class="breadcrumb-item"><a href="/project-list">PROJECTS</a></li>
        <li class="breadcrumb-item"><a href="/task-list">TASKS</a></li>
    </ol>
</nav>
<c:url value="/project-create" var="var"/>
    <form id="formId" action="${var}" method="POST">
            <div class="form-group">
                <label for="name">NAME</label>
                <input type="text" class="form-control" name="name" id="name">
             </div>
            <div class="form-group">
                <label for="description">DESCRIPTION</label>
                <input type="text" class="form-control" name="description" id="description">
            </div>
            <div class="form-group">
                <label for="status">STATUS</label>
                <select class="form-control" name="status" id="status">
                       <option>PLANNED</option>
                       <option>IN_PROCESS</option>
                       <option>READY</option>
                </select>
            </div>
            <div class="form-group">
                  <label for="dateOfBegin">DATE OF BEGIN</label>
                  <input type="date" class="form-control" name="dateOfBegin" id="dateOfBegin">
            </div>
        <button type="submit" class="btn btn-primary">CREATE</button>
    </form>
</body>
</html>