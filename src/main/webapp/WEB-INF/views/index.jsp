<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <style>
         .moveFromBorder {
                margin-top:20px;
                margin-left:50px;
                margin-right:1200px;
         }
    </style>
    <head>
         <meta charset="UTF-8">
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css"
                       integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
    </head>
    <body>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">MAIN</a></li>
                <li class="breadcrumb-item"><a href="/project-list">PROJECTS</a></li>
                <li class="breadcrumb-item"><a href="/task-list">TASKS</a></li>
            </ol>
        </nav>
        <h1 class="display-4 moveFromBorder">TASK MANAGER</h1>
        <h1 class="h3 alert alert-dark moveFromBorder"><a href="/project-list">PROJECT MANAGEMENT</a></h1>
        <h1 class="h3 alert alert-dark moveFromBorder"><a href="/task-list">TASK MANAGEMENT</a></h1>
    </body>
</html>
