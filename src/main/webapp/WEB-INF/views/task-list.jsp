<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <style>
         .moveFromBorder {
                margin-top:20px;
                margin-left:50px;
         }

         #smallTable{
            width:90%;
         }

    </style>
   <head>
      <title>TASK MANAGEMENT</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.0/css/bootstrap.min.css"
              integrity="sha384-SI27wrMjH3ZZ89r4o+fGIJtnzkAnFs3E4qz9DIYioCQ5l9Rd/7UAa8DHcaL8jkWt" crossorigin="anonymous">
   </head>
   <body>
   <nav aria-label="breadcrumb">
       <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="/">MAIN</a></li>
           <li class="breadcrumb-item"><a href="/project-list">PROJECTS</a></li>
           <li class="breadcrumb-item"><a href="/task-list">TASKS</a></li>
       </ol>
   </nav>
        <div>
        <h1 class="moveFromBorder">TASK MANAGEMENT</h1>
        <table class="table moveFromBorder" id="smallTable">
             <thead class="thead-dark">
                  <tr>
                       <th>ID</th>
                       <th>NAME</th>
                       <th>DESCRIPTION</th>
                       <th>STATUS</th>
                       <th>PROJECT</th>
                       <th>VIEW</th>
                       <th>EDIT</th>
                       <th>REMOVE</th>
                  </tr>
                       <c:forEach var="task" items="${taskList}">
                            <tr>
                                <td>${task.id}</td>
                                <td>${task.name}</td>
                                <td>${task.description}</td>
                                <td>${task.status}</td>
                                <td>${task.project.name}</td>
                                <td><a href="/task-view/${task.id}">VIEW</a></td>
                                <td><a href="/task-edit/${task.id}">EDIT</a></td>
                                <td><a href="/task-remove/${task.id}">REMOVE</a></td>
                            </tr>
                        </c:forEach>
             </thead>
        </table>
        <a href="/task-create" class="btn btn-primary moveFromBorder">CREATE TASK</a>
        </div>
   </body>
</html>