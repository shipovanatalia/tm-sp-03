package ru.shipova.tm.exception;

public class ProjectNotFoundException extends RuntimeException {
    public ProjectNotFoundException(String id) {
        super("Could not find project with id " + id);
    }
}
