package ru.shipova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_project")
public class Project extends AbstractEntity{
    @Column(name = "name")
    @Nullable private String name;

    @Column(name = "description")
    @NotNull
    private String description = "";

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @Nullable private List<Task> taskList;
}

