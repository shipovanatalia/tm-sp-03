package ru.shipova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.entity.AbstractEntity;
import ru.shipova.tm.entity.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {
}
