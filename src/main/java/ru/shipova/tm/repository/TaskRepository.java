package ru.shipova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.shipova.tm.entity.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {
}
