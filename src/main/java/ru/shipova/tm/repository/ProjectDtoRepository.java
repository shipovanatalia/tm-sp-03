package ru.shipova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.shipova.tm.dto.ProjectDTO;

public interface ProjectDtoRepository extends JpaRepository<ProjectDTO, String> {
}
