package ru.shipova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.shipova.tm.dto.TaskDTO;

public interface TaskDtoRepository extends JpaRepository<TaskDTO, String> {
}
