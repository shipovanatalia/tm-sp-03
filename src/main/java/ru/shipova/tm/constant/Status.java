package ru.shipova.tm.constant;

public enum Status {
    PLANNED("PLANNED"),
    IN_PROCESS("IN PROCESS"),
    READY("READY");

    private final String name;

    Status(final String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }
}
