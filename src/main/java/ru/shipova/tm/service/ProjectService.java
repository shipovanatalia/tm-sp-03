package ru.shipova.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shipova.tm.dto.ProjectDTO;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.exception.ProjectNotFoundException;
import ru.shipova.tm.repository.ProjectDtoRepository;
import ru.shipova.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@NoArgsConstructor
public class ProjectService {
    private ProjectRepository projectRepository;
    private ProjectDtoRepository projectDtoRepository;

    public ProjectService(ProjectRepository projectRepository, ProjectDtoRepository projectDtoRepository) {
        this.projectRepository = projectRepository;
        this.projectDtoRepository = projectDtoRepository;
    }

    public List<Project> getListProject() {
        List<Project> projectList = projectRepository.findAll();
        if (projectList == null) return new ArrayList<>();
        return projectList;
    }

    public List<ProjectDTO> getListProjectDTO() {
        List<ProjectDTO> projectList = projectDtoRepository.findAll();
        if (projectList == null) return new ArrayList<>();
        return projectList;
    }

    public Project create(@Nullable final Project project) {
        project.setId(UUID.randomUUID().toString());
        project.setDateOfCreate(new Date());
        return projectRepository.save(project);
    }

    public void remove(@Nullable final Project project) {
        projectRepository.delete(project);
    }

    @NotNull
    public Project findOne(@Nullable final String id) {
        return projectRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));
    }

    @NotNull
    public ProjectDTO findOneDTO(@Nullable final String id) {
        return projectDtoRepository.findById(id).orElseThrow(() -> new ProjectNotFoundException(id));
    }

    public void edit(@Nullable final Project project) {
        projectRepository.save(project);
    }

    public ProjectDTO editDTO(@Nullable final ProjectDTO projectDTO) {
        return projectDtoRepository.save(projectDTO);
    }
}
