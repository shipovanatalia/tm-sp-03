package ru.shipova.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shipova.tm.dto.TaskDTO;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.repository.TaskDtoRepository;
import ru.shipova.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
@NoArgsConstructor
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskDtoRepository taskDtoRepository;

    @NotNull
    public List<Task> getListTask() {
        List<Task> taskList = taskRepository.findAll();
        if (taskList == null) return new ArrayList<>();
        return taskList;
    }

    @Nullable
    public Task findOne(@Nullable final String id){
        return taskRepository.findById(id).orElse(null);
    }

    public void create(final @Nullable Task task) {
        task.setId(UUID.randomUUID().toString());
        task.setDateOfCreate(new Date());
        taskRepository.save(task);
    }

    public void remove(@Nullable final Task task) {
        taskRepository.delete(task);
    }

    public void edit(@Nullable final Task task){
        taskRepository.save(task);
    }

    public void editDTO(@Nullable final TaskDTO taskDTO){
        taskDtoRepository.save(taskDTO);
    }
}
