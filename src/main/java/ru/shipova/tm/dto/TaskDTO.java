package ru.shipova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_task1")
public class TaskDTO extends AbstractEntityDTO {
    @Column(name = "project_id")
    @Nullable
    private String projectId;

    @Column(name = "name")
    @Nullable
    private String name;

    @Column(name = "description")
    @Nullable
    private String description;
}
