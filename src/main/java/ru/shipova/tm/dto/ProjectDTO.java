package ru.shipova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_project")
public class ProjectDTO extends AbstractEntityDTO {
    @Column(name = "name")
    @Nullable
    private String name;

    @Column(name = "description")
    @NotNull
    private String description = "";
}
