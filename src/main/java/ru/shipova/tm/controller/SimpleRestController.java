package ru.shipova.tm.controller;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.shipova.tm.dto.ProjectDTO;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.service.ProjectService;

import java.util.List;

@RestController
public class SimpleRestController {

    @Nullable
    ProjectService projectService;

    public SimpleRestController(@Nullable ProjectService projectService) {
        this.projectService = projectService;
    }

    @RequestMapping(value = "/project", method = RequestMethod.GET)
    public Project project(){
        return new Project();
    }

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public List<ProjectDTO> projects() {
        List<ProjectDTO> projectList = projectService.getListProjectDTO();
        return projectList;
    }
}
