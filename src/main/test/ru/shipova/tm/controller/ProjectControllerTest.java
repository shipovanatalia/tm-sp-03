package ru.shipova.tm.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.shipova.tm.dto.ProjectDTO;
import ru.shipova.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProjectControllerTest {

    private ProjectController projectController;
    private ProjectService projectService;

    @BeforeEach
    void setUp() {
        projectService = mock(ProjectService.class);
        projectController = new ProjectController(projectService);
    }

    @Test
    void getProjectList() {
        List<ProjectDTO> projectList = new ArrayList<>();
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("project");
        projectList.add(projectDTO);
        when(projectService.getListProjectDTO()).thenReturn(projectList);

        List<ProjectDTO> result = projectController.getProjectList();

        assertArrayEquals(projectList.toArray(), result.toArray());
    }

    @Test
    void editProject() {
    }

    @Test
    void getProject() {
    }

    @Test
    void deleteProject() {
    }

    @Test
    void createProject() {
    }
}