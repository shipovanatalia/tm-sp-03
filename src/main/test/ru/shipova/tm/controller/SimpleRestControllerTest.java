package ru.shipova.tm.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.shipova.tm.dto.ProjectDTO;
import ru.shipova.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SimpleRestControllerTest {

    private SimpleRestController controller;
    private ProjectService service;

    @BeforeEach
    void setUp() {
        service = mock(ProjectService.class);
        controller = new SimpleRestController(service);
    }

    @Test
    void projects() {
        List<ProjectDTO> projects = new ArrayList<>();
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("project");
        projects.add(projectDTO);
        when(service.getListProjectDTO()).thenReturn(projects);

        List<ProjectDTO> result = controller.projects();

        assertArrayEquals(projects.toArray(), result.toArray());
    }
}